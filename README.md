# hexmap-editor

This is a Dungeons and Dragons hexmap editor and remote-play tool. The Dungeon Master can create maps with tiles (the general terrain in a place) and objects (some special stuff like villages, cities, big mountains,...), which is used using my homebrew rules for exploring/traversing unknown terrain.

See the README files in client/ and server/ to see how to start and use each one.