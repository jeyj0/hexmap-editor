const WebSocket = require('ws');
const { v4: uuid } = require('uuid');
const randomWords = require('random-words')

const errorCodes = {
  authentication: { code: 1, message: 'Authentication error.' },
  serverclose: { code: 2, message: 'Server closed.' },
}

function noop() {}

function heartbeat() {
  this.isAlive = true
}

const server = new WebSocket.Server({ port: 8083 });

const gameNames = new Map()
const games = new Map()
const clientSockets = new Map()

server.on('connection', function connection(ws, _request) {
  ws.isAlive = true;

  const id = uuid()
  clientSockets.set(id, ws)

  console.log('New client connected. Id:', id)

  const listeners = createListeners(id, ws)
  ws.on('message', listeners.message)
  ws.on('error', listeners.error)
  ws.on('close', listeners.close)
  ws.on('pong', heartbeat)

  send(ws, { type: 'set-id', id })
})

const interval = setInterval(function ping() {
  server.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate()

    ws.isAlive = false
    ws.ping(noop)
  })
}, 30000)

server.on('close', function onServerClose() {
  clearInterval(interval)
  Array.from(clientSockets.values()).forEach(ws => {
    ws.close(errorCodes.serverclose.code, errorCodes.serverclose.message)
  })
})

function send(socket, data) {
  socket.send(JSON.stringify(data))
}

function createListeners(id, ws) {
  function onMessage(data) {
    try {
      console.debug('message:', data)
      const message = JSON.parse(data)

      // authenticate the request (very basic right now)
      if (message.id !== id) {
        ws.close(errorCodes.authentication.code, errorCodes.authentication.message)
      }

      switch (message.type) {
        case 'start-game':
          // client starts a game as GM
          games.set(id, [])

          let gameName = null
          while (gameNames.has(gameName) || gameName === null) {
            gameName = randomWords({ exactly: 3, join: ' ' })
          }
          gameNames.set(gameName, id)

          send(ws, { type: 'game-name-set', gameName }) // CLIENT MSG TYPE

          break;
        case 'join-game':
          const gmId = gameNames.get(message.gameName)
          if (gmId === undefined) {
            send(ws, { type: 'game-not-found', gameName: message.gameName }) // CLIENT MSG TYPE
          } else {
            games.set(gmId, games.get(gmId).concat(id))
            const gmSocket = clientSockets.get(gmId)
            send(gmSocket, { type: 'new-player', gameName: message.gameName, socketId: id }) // CLIENT MSG TYPE
            send(ws, { type: 'game-name-id', gameName: message.gameName, gameId: gmId }) // CLIENT MSG TYPE
          }
          break;
        case 'broadcast-by-gm':
          const players = games.get(id)

          if (players === undefined) break;

          for (const playerId of players) {
            const socket = clientSockets.get(playerId)
            if (socket !== undefined) {
              send(socket, message.message)
            }
          }
          break;
        case 'send-to':
          const socket = clientSockets.get(message.receiverId)
          send(socket, message.message)
          break;
      }
    } catch(e) {
      console.error(e)
    }
  }

  function onError(err) {
    console.error('Websocket error.')
    console.error(err)
  }

  function onClose(code, message) {
    console.debug('Connection closed.', { code, id, message })
    clientSockets.delete(id)
    if (games.has(id)) games.delete(id)

    for (const [gm, players] of games) {
      if (players.includes(id)) {
        games.set(gm, players.filter((playerId) => playerId !== id))
      }
    }
  }

  return {
    message: onMessage,
    error: onError,
    close: onClose,
  }
}

