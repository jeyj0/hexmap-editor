const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: [
    "src/**/*.js",
    "src/**/*.jsx",
    "src/**/*.ts",
    "src/**/*.tsx",
    "public/**/*.html",
  ],
  theme: {
    boxShadow: {
      ...defaultTheme.boxShadow,
      outline: "0 0 0 3px rgba(88, 24, 13, 0.5)",
    },
    extend: {
      fontSize: {
        mapLabel: "10px",
      },
      fontFamily: {
        body: ["Scaly Sans"],
        quote: ["Scaly Sans Caps"],
        title: ["Nodesto Caps Condensed"],
        heading: ["Mr Eaves Small Caps"],
        table: ["Bookinsanity"],
        tabletitle: ["Zatanna Misdirection"],
        dropcaps: ["Solbera Imitation"],
      },
      colors: {
        dnd: {
          bg: "#eee8d0",
          header: "#58180D",
          hr: "#9c2b1b",
          border: "#c9ad6a",
        },
        backdrop: {
          light: "rgba(255, 255, 255, 0.5)",
          dark: "rgba(0, 0, 0, 0.5)",
        },
      },
    },
  },
  variants: {
    backgroundColor: ({ after }) => after(["active"]),
  },
  plugins: [],
};
