export const size = 10;
export const w = Math.sqrt(3) * size;
export const h = 2 * size;
export const zeroZeroHex = { x: w / 2 + 1, y: h / 2 + 1 };

export const websocketHost = window.location.hostname;
export const websocketPort = 8083;

export enum Mode {
  Select,
  Paint,
  Erase,
  FogOfWar,
}
export const modes: [Mode, string, string][] = [
  [Mode.Select, "Select", "S"],
  [Mode.Paint, "Paint", "P"],
  [Mode.Erase, "Erase", "E"],
  [Mode.FogOfWar, "Fog of War", "F"],
];

export enum Tab {
  Tiles,
  Objects,
  Settings,
}
export const tabs: [Tab, string][] = [
  [Tab.Tiles, "Tiles"],
  [Tab.Objects, "Objects"],
  [Tab.Settings, "Settings"],
];

export enum MapType {
  Exploration = "Exploration",
  Combat = "Combat",
}
export const mapTypes: Map<string, MapType> = new Map();
mapTypes.set("Exploration", MapType.Exploration);
mapTypes.set("Combat", MapType.Combat);

export function isMapType(obj: any): obj is MapType {
  return Array.from(mapTypes.values()).includes(obj);
}

export function isKToVEntries<K, V>(
  entries: any[],
  isK: (o: any) => o is K,
  isV: (o: any) => o is V
): entries is [K, V][] {
  return entries.every((e) => {
    return (
      Array.isArray(e) &&
      e.length === 2 &&
      isKToVEntry(e as [any, any], isK, isV)
    );
  });
}

export function isKToVEntry<K, V>(
  entry: [any, any],
  isK: (o: any) => o is K,
  isV: (o: any) => o is V
): entry is [K, V] {
  return isK(entry[0]) && isV(entry[1]);
}

export function isKToVMap<K, V>(
  obj: any,
  isK: (o: any) => o is K,
  isV: (o: any) => o is V
): obj is Map<K, V> {
  if (!(obj instanceof Map)) {
    return false;
  }

  for (const keyValue of obj) {
    const [key, value] = keyValue;
    if (!isK(key) || !isV(value)) {
      return false;
    }
  }

  return true;
}

export interface ObjectGroup {
  id: string;
  name: string;
  groups: string[];
  objects: string[];
  mapObjectTemplate: {
    showNameOnMap: boolean | null;
    nameColor: string | null;
    color: string | null;
    dmOnly?: boolean;
  };
}

function isMapObjectTemplate(
  obj: any
): obj is {
  showNameOnMap: boolean | null;
  nameColor: string | null;
  color: string | null;
} {
  if (!objHasOwnProperties(obj, ["showNameOnMap", "nameColor", "color"])) {
    return false;
  }

  const baseObj = obj as { showNameOnMap: any; nameColor: any; color: any };

  return (
    (isBoolean(baseObj.showNameOnMap) || baseObj.showNameOnMap === null) &&
    (isString(baseObj.nameColor) || baseObj.nameColor === null) &&
    (isString(baseObj.color) || baseObj.color === null)
  );
}

export function isObjectGroup(obj: any): obj is ObjectGroup {
  if (
    !objHasOwnProperties(obj, [
      "id",
      "name",
      "groups",
      "objects",
      "mapObjectTemplate",
    ])
  ) {
    return false;
  }

  const baseObj = obj as {
    id: any;
    name: any;
    groups: any;
    objects: any;
    mapObjectTemplate: any;
  };

  return (
    isString(baseObj.id) &&
    isString(baseObj.name) &&
    isMapObjectTemplate(baseObj.mapObjectTemplate) &&
    Array.isArray(baseObj.groups) &&
    Array.isArray(baseObj.objects) &&
    baseObj.groups.every(isString) &&
    baseObj.objects.every(isString)
  );
}

export interface MapObject {
  id: string;
  name: string;
  showNameOnMap: boolean | null;
  dmOnly?: boolean;
  nameColor: string | null;
  color: string | null;
  position: {
    q: number;
    r: number;
  };
}

export interface AssembledMapObject {
  id: string;
  name: string;
  showNameOnMap: boolean;
  dmOnly: boolean;
  nameColor: string;
  color: string;
  position: {
    q: number;
    r: number;
  };
}

export function isBoolean(obj: any): obj is boolean {
  return typeof obj === "boolean";
}

export function isNumber(obj: any): obj is number {
  return typeof obj === "number";
}

export function isPosition(obj: any): obj is { q: number; r: number } {
  if (!objHasOwnProperties(obj, ["q", "r"])) {
    return false;
  }

  const baseObj = obj as { q: any; r: any };

  return isNumber(baseObj.q) && isNumber(baseObj.r);
}

export function isMapObject(obj: any): obj is MapObject {
  if (
    !objHasOwnProperties(obj, [
      "id",
      "name",
      "showNameOnMap",
      "nameColor",
      "color",
      "position",
    ])
  ) {
    return false;
  }

  const baseObj = obj as {
    id: any;
    name: any;
    showNameOnMap: any;
    nameColor: any;
    color: any;
    position: any;
  };

  return (
    isString(baseObj.id) &&
    isString(baseObj.name) &&
    (isBoolean(baseObj.showNameOnMap) || baseObj.showNameOnMap === null) &&
    (isString(baseObj.nameColor) || baseObj.nameColor === null) &&
    (isString(baseObj.color) || baseObj.color === null) &&
    isPosition(baseObj.position)
  );
}

export interface Tile {
  name: string;
  symbol?: string;
  fillColor: string;
  color: string;
}

export function isString(o: any): o is string {
  return typeof o === "string";
}

export function objHasOwnProperties(obj: any, properties: string[]): boolean {
  for (const property of properties) {
    if (!Object.hasOwnProperty.call(obj, property)) {
      return false;
    }
  }
  return true;
}

export function isTile(obj: any): obj is Tile {
  if (!objHasOwnProperties(obj, ["name", "fillColor", "color"])) {
    return false;
  }

  const baseObj = obj as { name: any; fillColor: any; color: any };

  if (
    typeof baseObj.name === "string" &&
    typeof baseObj.fillColor === "string" &&
    typeof baseObj.color === "string"
  ) {
    if (Object.hasOwnProperty.call(obj, "symbol")) {
      return typeof obj.symbol === "string";
    }
    return true;
  }

  return false;
}

export const tileStyleTagId = "customTileStyles";
export const tileClassPrefix = "tile_";

export const objectClassPrefix = "object_";

export const localStorageMapPrefix = "hexmap_";
export const localStorageMapListKey = "hexmap_editor_maps";
