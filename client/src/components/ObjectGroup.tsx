import React, { useState, useEffect } from "react";
import { MapObject } from "../const";
import { observer, useLocalStore } from "mobx-react-lite";
import ObjectEditor from "./ObjectEditor";
import { useStore } from "../hooks/useStore";

interface ObjectGroupProps {
  onBack?: () => any;
  next: (groupId: string) => any;
  objectGroupId: string;
  path: string;
}

export default observer(function ObjectGroup({
  path,
  onBack,
  next,
  objectGroupId,
}: ObjectGroupProps) {
  const store = useStore();
  const objectGroup = store.getObjectGroup(objectGroupId);

  const [isEditingName, setIsEditingName] = useState(false);
  const [editingName, setEditingName] = useState(objectGroup?.name ?? "");

  const objectEditorStore = useLocalStore<{ obj: null | MapObject }>(() => ({
    obj: null,
  }));

  useEffect(() => {
    setEditingName(objectGroup?.name ?? "");
  }, [objectGroup]);

  if (objectGroup === undefined) {
    return null;
  }

  function onSubmit(evt?: React.FormEvent<HTMLFormElement>) {
    if (evt !== undefined) {
      evt.preventDefault();
    }

    if (objectGroup !== undefined && editingName.length > 0) {
      setIsEditingName(false);
      objectGroup.name = editingName;
    }
  }

  return (
    <div className="grid">
      <div className="space-x-2">
        {onBack !== undefined ? (
          <button className="text-dnd-header" onClick={() => onBack()}>
            Back
          </button>
        ) : null}
        <span className="text-sm font-heading">{path}</span>
      </div>
      {isEditingName ? (
        <form onSubmit={onSubmit}>
          <input
            type="text"
            className="nameEdit"
            autoFocus
            value={editingName}
            onBlur={() => onSubmit()}
            onChange={(evt) => setEditingName(evt.target.value)}
          />
        </form>
      ) : (
        <button
          onClick={() => setIsEditingName(true)}
          className="font-bold text-left text-dnd-header font-heading"
        >
          {objectGroup.name}
        </button>
      )}
      <div>
        <ul className="text-dnd-header">
          {objectGroup.groups.map((groupId, idx) => {
            const group = store.getObjectGroup(groupId);

            if (group === undefined) {
              return null;
            }

            return (
              <li className="list-disc list-inside" key={idx}>
                <button onClick={() => next(groupId)}>{group.name}</button>
              </li>
            );
          })}
        </ul>
        <button
          onClick={() => {
            const group = store.createObjectGroup(objectGroupId);
            next(group.id);
          }}
          className="danger"
        >
          New Object Group
        </button>
      </div>
      <hr className="my-4" />
      {objectGroup.id === store.rootObjectGroup.id ? null : (
        <label className="block space-x-2">
          <input
            type="checkbox"
            checked={objectGroup.mapObjectTemplate.color !== null}
            onChange={(evt) => {
              if (evt.target.checked) {
                objectGroup.mapObjectTemplate.color = "#ff0000";
              } else {
                objectGroup.mapObjectTemplate.color = null;
              }
            }}
          />
          <span>Override "Background Color"</span>
        </label>
      )}
      {objectGroup.mapObjectTemplate.color === null ? null : (
        <label className="block space-x-2">
          <input
            type="color"
            value={objectGroup.mapObjectTemplate.color}
            onChange={(evt) =>
              (objectGroup.mapObjectTemplate.color = evt.target.value)
            }
          />
          <span>Background Color</span>
        </label>
      )}
      {objectGroup.id === store.rootObjectGroup.id ? null : (
        <label className="block space-x-2">
          <input
            type="checkbox"
            checked={objectGroup.mapObjectTemplate.dmOnly !== undefined}
            onChange={(evt) => {
              if (evt.target.checked) {
                objectGroup.mapObjectTemplate.dmOnly = false;
              } else {
                objectGroup.mapObjectTemplate.dmOnly = undefined;
              }
            }}
          />
          <span>Override "DM Only"</span>
        </label>
      )}
      {objectGroup.mapObjectTemplate.dmOnly === undefined ? null : (
        <label className="block space-x-2">
          <input
            type="checkbox"
            checked={objectGroup.mapObjectTemplate.dmOnly}
            onChange={(evt) =>
              (objectGroup.mapObjectTemplate.dmOnly = evt.target.checked)
            }
          />
          <span>DM Only</span>
        </label>
      )}
      {objectGroup.id === store.rootObjectGroup.id ? null : (
        <label className="block space-x-2">
          <input
            type="checkbox"
            checked={objectGroup.mapObjectTemplate.showNameOnMap !== null}
            onChange={(evt) => {
              if (evt.target.checked) {
                objectGroup.mapObjectTemplate.showNameOnMap = false;
              } else {
                objectGroup.mapObjectTemplate.showNameOnMap = null;
              }
            }}
          />
          <span>Override "Show name on map"</span>
        </label>
      )}
      {objectGroup.mapObjectTemplate.showNameOnMap === null ? null : (
        <label className="block space-x-2">
          <input
            type="checkbox"
            checked={objectGroup.mapObjectTemplate.showNameOnMap}
            onChange={(evt) =>
              (objectGroup.mapObjectTemplate.showNameOnMap = evt.target.checked)
            }
          />
          <span>Show name on map</span>
        </label>
      )}
      {objectGroup.id === store.rootObjectGroup.id ? null : (
        <label className="block space-x-2">
          <input
            type="checkbox"
            checked={objectGroup.mapObjectTemplate.nameColor !== null}
            onChange={(evt) => {
              if (evt.target.checked) {
                objectGroup.mapObjectTemplate.nameColor = "#ffffff";
              } else {
                objectGroup.mapObjectTemplate.nameColor = null;
              }
            }}
          />
          <span>Override "Text Color"</span>
        </label>
      )}
      {objectGroup.mapObjectTemplate.nameColor === null ? null : (
        <label className="block space-x-2">
          <input
            type="color"
            value={objectGroup.mapObjectTemplate.nameColor}
            onChange={(evt) =>
              (objectGroup.mapObjectTemplate.nameColor = evt.target.value)
            }
          />
          <span>Text Color</span>
        </label>
      )}
      <hr className="my-4" />
      <div>
        <ul className="grid grid-cols-2 text-dnd-header">
          {objectGroup.objects.map((id, idx) => {
            const object = store.getObject(id);

            if (object === undefined) {
              return null;
            }

            return (
              <li className="list-disc list-inside" key={idx}>
                <button onClick={() => (objectEditorStore.obj = object)}>
                  {object.name}
                </button>
              </li>
            );
          })}
        </ul>
        <button
          onClick={() => {
            store.createObject(objectGroupId);
          }}
          className="danger"
        >
          New Object
        </button>
      </div>
      <div>
        {objectEditorStore.obj !== null ? (
          <>
            <hr className="my-4" />
            <ObjectEditor obj={objectEditorStore.obj} />
          </>
        ) : null}
      </div>
    </div>
  );
});
