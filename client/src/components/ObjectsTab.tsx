import React, { useState } from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "../hooks/useStore";
import ObjectGroup from "./ObjectGroup";

export default observer(function ObjectsTab() {
  const store = useStore();
  const [openGroup, setOpenGroup] = useState(store.rootObjectGroup.id);
  const [previousGroups, setPreviousGroups] = useState<string[]>([]);

  const path = previousGroups
    .map((g) => store.getObjectGroup(g)?.name)
    .filter((n) => n !== undefined)
    .join("/");

  const onBack =
    previousGroups.length > 0
      ? () => {
          setOpenGroup(previousGroups[previousGroups.length - 1]);
          const newPreviousGroups = [...previousGroups];
          newPreviousGroups.length = newPreviousGroups.length - 1;
          setPreviousGroups(newPreviousGroups);
        }
      : undefined;

  function next(groupId: string) {
    setPreviousGroups([...previousGroups, openGroup]);
    setOpenGroup(groupId);
  }

  return (
    <div>
      <ObjectGroup
        onBack={onBack}
        next={next}
        objectGroupId={openGroup}
        path={(path === "" ? "<root>" : path) + "/"}
      />
    </div>
  );
});
