import React from "react";
import { Mode, modes } from "../const";
import { observer } from "mobx-react-lite";
import { useStore } from "../hooks/useStore";

export default observer(function LeftPanel() {
  const store = useStore();

  return (
    <div className="p-2 bg-gray-800 dnd-bg">
      {modes.map(([mode, modeName, modeSymbol]) => {
        const classes = [
          "block py-2 px-4 rounded focus:outline-none border-2 border-transparent",
          mode === store.ui.selectedMode
            ? "border-2 border-dnd-border text-dnd-header"
            : "",
        ].join(" ");

        return (
          <button
            className={classes}
            key={modeName}
            onClick={() => (store.ui.selectedMode = mode)}
            title={modeName}
          >
            {modeSymbol}
          </button>
        );
      })}
      {store.ui.selectedMode !== Mode.FogOfWar ? (
        <label className="block my-2 text-center">
          <input
            type="checkbox"
            checked={store.ui.renderFog}
            onChange={(evt) => (store.ui.renderFog = evt.target.checked)}
          />
          <span className="block text-gray-800" style={{ marginTop: "-0.7em" }}>
            Fog
          </span>
        </label>
      ) : null}
    </div>
  );
});
