import React, { useEffect, useState } from "react";
import { MapObject } from "../const";
import { observer } from "mobx-react-lite";

interface ObjectEditorProps {
  obj: MapObject;
}

export default observer(function ObjectEditor({ obj }: ObjectEditorProps) {
  const [isEditingName, setIsEditingName] = useState(false);
  const [editingName, setEditingName] = useState(obj.name);

  useEffect(() => {
    setEditingName(obj.name);
  }, [obj.name, setEditingName]);

  function onSubmit(evt?: React.FormEvent<HTMLFormElement>) {
    if (evt !== undefined) {
      evt.preventDefault();
    }

    if (editingName.length > 0) {
      obj.name = editingName;
      setIsEditingName(false);
    }
  }

  return (
    <>
      {isEditingName ? (
        <form onSubmit={onSubmit}>
          <input
            className="nameEdit"
            autoFocus
            value={editingName}
            onBlur={() => onSubmit()}
            onChange={(evt) => setEditingName(evt.target.value)}
          />
        </form>
      ) : (
        <button
          className="font-bold text-dnd-header font-heading"
          onClick={() => setIsEditingName(true)}
        >
          {obj.name}
        </button>
      )}
      <span className="block">
        Q: {obj.position.q} | R: {obj.position.r}
      </span>
      <label className="block border-t-2 space-x-2 border-dnd-header">
        <input
          type="checkbox"
          checked={obj.color !== null}
          onChange={(evt) => {
            if (evt.target.checked) {
              obj.color = "#ff0000";
            } else {
              obj.color = null;
            }
          }}
        />
        <span>Override "Background Color"</span>
      </label>
      {obj.color === null ? null : (
        <label className="block space-x-2">
          <input
            type="color"
            value={obj.color}
            onChange={(evt) => (obj.color = evt.target.value)}
          />
          <span>Background Color</span>
        </label>
      )}
      <label className="block border-t-2 space-x-2 border-dnd-header">
        <input
          type="checkbox"
          checked={obj.showNameOnMap !== null}
          onChange={(evt) => {
            if (evt.target.checked) {
              obj.showNameOnMap = false;
            } else {
              obj.showNameOnMap = null;
            }
          }}
        />
        <span>Override "Show name on map"</span>
      </label>
      {obj.showNameOnMap === null ? null : (
        <label className="block space-x-2">
          <input
            type="checkbox"
            checked={obj.showNameOnMap}
            onChange={(evt) => (obj.showNameOnMap = evt.target.checked)}
          />
          <span>Show name on map</span>
        </label>
      )}
      <label className="block border-t-2 space-x-2 border-dnd-header">
        <input
          type="checkbox"
          checked={obj.dmOnly !== undefined}
          onChange={(evt) => {
            if (evt.target.checked) {
              obj.dmOnly = false;
            } else {
              obj.dmOnly = undefined;
            }
          }}
        />
        <span>Override "DM Only"</span>
      </label>
      {obj.dmOnly === undefined ? null : (
        <label className="block space-x-2">
          <input
            type="checkbox"
            checked={obj.dmOnly}
            onChange={(evt) => (obj.dmOnly = evt.target.checked)}
          />
          <span>DM Only</span>
        </label>
      )}
      <label className="block border-t-2 space-x-2 border-dnd-header">
        <input
          type="checkbox"
          checked={obj.nameColor !== null}
          onChange={(evt) => {
            if (evt.target.checked) {
              obj.nameColor = "#ffffff";
            } else {
              obj.nameColor = null;
            }
          }}
        />
        <span>Override "Text Color"</span>
      </label>
      {obj.nameColor === null ? null : (
        <label className="block space-x-2">
          <input
            type="color"
            value={obj.nameColor}
            onChange={(evt) => (obj.nameColor = evt.target.value)}
          />
          <span>Text Color</span>
        </label>
      )}
    </>
  );
});
