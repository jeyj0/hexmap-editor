import React from "react";
import { w, h } from "../const";
import Hex from "./Hex";

interface SingleHexProps {
  className?: string;
  tileId?: string;
  onClick?: (evt?: React.MouseEvent<SVGElement>) => any;
}

export default function SingleHex({
  className,
  tileId,
  onClick,
}: SingleHexProps) {
  const classes = ["focus:outline-none", className].join(" ");

  return (
    <svg
      viewBox={`0 0 ${w + 2} ${h + 2}`}
      style={{ width: "3rem" }}
      className={classes}
      tabIndex={0}
      onKeyPress={(evt) => {
        if (evt.key === "Enter" && onClick !== undefined) {
          onClick();
        }
      }}
    >
      <Hex q={0} r={0} tileId={tileId} onClick={onClick} uiHex={true} />
    </svg>
  );
}
