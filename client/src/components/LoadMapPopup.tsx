import React, { useState } from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "../hooks/useStore";

interface LoadMapPopupProps {
  popupId: string;
}

export default observer(function LoadMapPopup({ popupId }: LoadMapPopupProps) {
  const store = useStore();

  const [selectedMapId, setSelectedMapId] = useState<string | null>(null);

  const isSubmitDisabled = selectedMapId === null;
  const submitButtonClasses =
    "p-2 border-2 rounded " +
    (isSubmitDisabled
      ? "border-gray-900 bg-gray-800 cursor-not-allowed"
      : "bg-dnd-hr text-dnd-bg border-dnd-header focus:outline-none focus:shadow-outline active:bg-dnd-header");

  return (
    <form
      onSubmit={(evt) => {
        evt.preventDefault();

        if (selectedMapId !== null) {
          store.closePopup(popupId);
          store.loadMap(selectedMapId);
        }
      }}
      className="bg-gray-800 rounded"
    >
      <div className="p-4 text-dnd-bg">
        <label>
          Select Map to load...
          <select
            className="bg-backdrop-light text-gray-900 block p-2"
            defaultValue="placeholder"
            value={selectedMapId ?? undefined}
            onChange={(evt) => setSelectedMapId(evt.target.value)}
          >
            <option value="placeholder" disabled>
              Choose a map to load...
            </option>
            {store.savedMaps.map((map) => {
              return (
                <option key={map.id} value={map.id}>
                  {map.name}
                </option>
              );
            })}
          </select>
        </label>
      </div>
      <div className="p-2 text-right bg-gray-700 rounded-b space-x-2 font-quote">
        <button
          onClick={() => store.closePopup(popupId)}
          className="p-2 bg-gray-600 border-2 border-gray-800 rounded focus:outline-none focus:shadow-outline active:bg-gray-800"
        >
          Cancel
        </button>
        <button
          type="submit"
          disabled={selectedMapId === null}
          className={submitButtonClasses}
        >
          Load
        </button>
      </div>
    </form>
  );
});
