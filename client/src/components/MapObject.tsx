import React from "react";
import { zeroZeroHex, Mode, w, h, size, objectClassPrefix } from "../const";
import { useStore } from "../hooks/useStore";
import { observer } from "mobx-react-lite";

interface MapObjectProps {
  id: string;
}

export default observer(function MapObject({ id }: MapObjectProps) {
  const store = useStore();

  const mapObject = store.assembleMapObject(id);

  if (mapObject === undefined) {
    return null;
  }

  if (mapObject.dmOnly && store.ui.role !== "dungeonMaster") {
    return null;
  }

  const isInFog =
    store.fogOfWar.overrides.get(
      `${mapObject.position.q}|${mapObject.position.r}`
    ) ?? store.fogOfWar.default;
  const renderPlayerFog = isInFog && store.ui.role === "player";
  const renderMasterFog =
    isInFog &&
    store.ui.role === "dungeonMaster" &&
    (store.ui.selectedMode === Mode.FogOfWar || store.ui.renderFog);

  if (renderPlayerFog) {
    return null;
  }

  const rDiff = {
    x: (mapObject.position.r * w) / 2,
    y: mapObject.position.r * h * 0.75,
  };
  const qDiff = { x: mapObject.position.q * w, y: 0 };
  const totalDiff = { x: rDiff.x + qDiff.x, y: rDiff.y + qDiff.y };

  const center = {
    x: zeroZeroHex.x + totalDiff.x,
    y: zeroZeroHex.y + totalDiff.y,
  };

  const textBgOffset = 0.5;

  const classes = [
    "mapObjectGroup",
    objectClassPrefix + id,
    renderMasterFog ? "masterFog" : null,
  ]
    .filter((c) => c !== null)
    .join(" ");

  return (
    <g className={classes}>
      <circle
        cx={center.x}
        cy={center.y}
        r={size / 3}
        className={"mapObject " + objectClassPrefix + id}
        fill={mapObject.color}
        onMouseDown={() => {
          store.ui.draggingObject = id;
        }}
      />
      {mapObject.showNameOnMap || store.ui.role === "dungeonMaster" ? (
        <g className={mapObject.showNameOnMap ? "" : "name-tooltip"}>
          {[
            [-textBgOffset, -textBgOffset],
            [-textBgOffset, textBgOffset],
            [textBgOffset, -textBgOffset],
            [textBgOffset, textBgOffset],
            [-textBgOffset, 0],
            [textBgOffset, 0],
            [0, -textBgOffset],
            [0, textBgOffset],
          ].map(([xOff, yOff]) => (
            <text
              key={`${xOff}:${yOff}`}
              x={center.x - xOff}
              y={center.y - 5 + yOff}
              className="pointer-events-none text-mapLabel"
              textAnchor="middle"
              fill="black"
            >
              {mapObject.name}
            </text>
          ))}
          <text
            x={center.x}
            y={center.y - 5}
            className="pointer-events-none text-mapLabel"
            textAnchor="middle"
            fill={mapObject.nameColor}
          >
            {mapObject.name}
          </text>
        </g>
      ) : null}
    </g>
  );
});
