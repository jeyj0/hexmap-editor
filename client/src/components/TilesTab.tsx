import React from "react";
import { useStore } from "../hooks/useStore";
import { observer } from "mobx-react-lite";
import SingleHex from "./SingleHex";
import TileEditor from "./TileEditor";

export default observer(function TilesTab() {
  const store = useStore();

  return (
    <>
      <div className="grid grid-cols-6 place-items-center">
        {store.tilesList.map(({ id }) => {
          const classes = [
            "inline-block",
            id === store.ui.selectedTile ? "selectedHex" : "",
          ].join(" ");

          return (
            <SingleHex
              key={id}
              className={classes}
              tileId={id}
              onClick={() => (store.ui.selectedTile = id)}
            />
          );
        })}
        <SingleHex
          className="inline-block add-tile"
          onClick={() => store.addTile()}
        />
      </div>
      {store.ui.selectedTile ? (
        <>
          <hr className="my-4" />
          <TileEditor tileId={store.ui.selectedTile} />
        </>
      ) : null}
    </>
  );
});
