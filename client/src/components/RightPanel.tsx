import React, { useState } from "react";
import { Tab, tabs } from "../const";
import TilesTab from "./TilesTab";
import { observer } from "mobx-react-lite";
import Settings from "./Settings";
import ObjectsTab from "./ObjectsTab";

export default observer(function RightPanel() {
  const [activeTab, setActiveTab] = useState(Tab.Tiles);

  return (
    <div className="p-4 overflow-auto text-black bg-dnd-bg dnd-bg rightPanel">
      <div className="grid place-center">
        <div className="inline-block mb-2">
          {tabs.map(([tab, tabName]) => {
            const classes = [
              "border-b-2 border-transparent text-dnd-header px-4 font-heading",
              tab === activeTab ? "border-dnd-border font-bold" : "",
            ].join(" ");

            return (
              <button
                className={classes}
                key={tabName}
                onClick={() => setActiveTab(tab)}
              >
                {tabName}
              </button>
            );
          })}
        </div>
      </div>
      {(() => {
        switch (activeTab) {
          case Tab.Settings:
            return <Settings />;
          case Tab.Objects:
            return <ObjectsTab />;
          case Tab.Tiles:
            return <TilesTab />;
        }
      })()}
    </div>
  );
});
