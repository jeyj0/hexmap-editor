import React from "react";
import { Mode } from "../const";
import { useStore } from "../hooks/useStore";
import { w, h } from "../const";
import Hex from "./Hex";
import { observer } from "mobx-react-lite";
import MapObject from "./MapObject";

export default observer(function HexMap() {
  const store = useStore();

  const visibleHexCoords = [];
  for (let q = 0; q < store.ui.viewport.width; q++) {
    for (let r = 0; r < store.ui.viewport.height; r++) {
      visibleHexCoords.push({
        q: store.ui.viewport.offset.q + q - Math.floor(r / 2),
        r: store.ui.viewport.offset.r + r,
      });
    }
  }

  const viewBox = {
    minX: store.topLeftHex.x - (w / 2 + 1),
    minY: store.topLeftHex.y - (h / 2 + 1),
    width: store.ui.viewport.width * w + w / 2 + 1,
    height: (3 / 4) * h * store.ui.viewport.height + h / 4 + 1,
  };

  return (
    <div className="relative overflow-auto bg-black scrollbar-dark full-width">
      <svg
        viewBox={`${viewBox.minX} ${viewBox.minY} ${viewBox.width} ${viewBox.height}`}
        className={store.ui.animateFog ? "fogAnimated" : ""}
      >
        {visibleHexCoords.map(({ q, r }) => {
          const tileId = store.getAt(q, r) ?? "default";

          const onClick = (() => {
            if (store.ui.role === "dungeonMaster") {
              if (store.ui.draggingObject === null) {
                switch (store.ui.selectedMode) {
                  case Mode.Paint:
                    if (store.ui.selectedTile !== null) {
                      return () =>
                        store.setAt(q, r, store.ui.selectedTile as string);
                    }
                    break;
                  case Mode.Erase:
                    return () => store.deleteAt(q, r);
                  case Mode.FogOfWar:
                    return (evt: React.MouseEvent<SVGPolygonElement>) => {
                      if (evt.buttons === 1) {
                        // primary mouse button
                        store.fogOfWar.overrides.set(`${q}|${r}`, true);
                      } else if (evt.buttons === 2) {
                        // secondary mouse button
                        store.fogOfWar.overrides.set(`${q}|${r}`, false);
                      }
                    };
                }
                return () => {};
              } else {
                return (_: any, position: { q: number; r: number }) => {
                  if (store.ui.draggingObject !== null) {
                    const draggingObject = store.getObject(
                      store.ui.draggingObject
                    );

                    if (draggingObject !== undefined) {
                      draggingObject.position = position;
                    }
                  }
                };
              }
            }

            return () => {};
          })();

          const onMouseUp = (() => {
            if (store.ui.role === "dungeonMaster") {
              if (store.ui.draggingObject === null) {
                return () => {};
              }

              return (_evt: any, position: { q: number; r: number }) => {
                if (store.ui.draggingObject !== null) {
                  const draggingObject = store.getObject(store.ui.draggingObject);

                  if (draggingObject !== undefined) {
                    draggingObject.position = position;
                  }
                }
              };
            }
            return () => {}
          })();

          return (
            <Hex
              q={q}
              r={r}
              key={`${q},${r}`}
              tileId={tileId}
              onClick={onClick}
              onMouseDownOver={onClick}
              onMouseUp={onMouseUp}
            />
          );
        })}
        {store.objectsList.map((o) => {
          return <MapObject key={o.id} id={o.id} />;
        })}
      </svg>
      <button
        onClick={() => {
          store.ui.viewport.offset.q--;
        }}
        className="bg-gray-700 navButton navButton-x navButton-l text-dnd-bg active:bg-gray-800"
      >
        {"<"}
      </button>
      <button
        onClick={() => {
          store.ui.viewport.offset.q++;
        }}
        className="bg-gray-700 navButton navButton-x navButton-r text-dnd-bg active:bg-gray-800"
      >
        {">"}
      </button>
      <button
        onClick={() => {
          store.ui.viewport.offset.q++;
          store.ui.viewport.offset.r -= 2;
        }}
        className="bg-gray-700 navButton navButton-y navButton-t text-dnd-bg active:bg-gray-800"
      >
        {"^"}
      </button>
      <button
        onClick={() => {
          store.ui.viewport.offset.q--;
          store.ui.viewport.offset.r += 2;
        }}
        className="bg-gray-700 navButton navButton-y navButton-b text-dnd-bg active:bg-gray-800"
      >
        {"v"}
      </button>
    </div>
  );
});
