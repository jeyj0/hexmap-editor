import React from "react";
import { zeroZeroHex, Mode, w, h, tileClassPrefix } from "../const";
import { useStore } from "../hooks/useStore";
import { observer } from "mobx-react-lite";

interface HexProps {
  q: number;
  r: number;
  tileId?: string;
  onClick?: (
    evt: React.MouseEvent<SVGPolygonElement>,
    position: { q: number; r: number }
  ) => any;
  onMouseDownOver?: (
    evt: React.MouseEvent<SVGPolygonElement>,
    position: { q: number; r: number }
  ) => any;
  onMouseUp?: (
    evt: React.MouseEvent<SVGPolygonElement>,
    position: { q: number; r: number }
  ) => any;
  uiHex?: boolean;
}

export default observer(function Hex({
  q,
  r,
  tileId,
  onClick,
  onMouseDownOver,
  onMouseUp,
  uiHex,
}: HexProps) {
  const store = useStore();
  const tile = tileId !== undefined ? store.tiles.get(tileId) : undefined;
  const isInFog =
    !(uiHex ?? false) &&
    (store.fogOfWar.overrides.get(`${q}|${r}`) ?? store.fogOfWar.default);
  const renderPlayerFog = isInFog && store.ui.role === "player";
  const renderMasterFog =
    isInFog &&
    store.ui.role === "dungeonMaster" &&
    (store.ui.selectedMode === Mode.FogOfWar || store.ui.renderFog);

  const rDiff = { x: (r * w) / 2, y: r * h * 0.75 };
  const qDiff = { x: q * w, y: 0 };
  const totalDiff = { x: rDiff.x + qDiff.x, y: rDiff.y + qDiff.y };

  const center = {
    x: zeroZeroHex.x + totalDiff.x,
    y: zeroZeroHex.y + totalDiff.y,
  };

  const top = { x: center.x, y: center.y - h / 2 };
  const topRight = { x: center.x + w / 2, y: center.y - h / 4 };
  const botRight = { x: center.x + w / 2, y: center.y + h / 4 };
  const bottom = { x: center.x, y: center.y + h / 2 };
  const topLeft = { x: center.x - w / 2, y: center.y - h / 4 };
  const botLeft = { x: center.x - w / 2, y: center.y + h / 4 };

  const pointsString = [top, topRight, botRight, bottom, botLeft, topLeft]
    .map((point) => {
      return `${point.x},${point.y}`;
    })
    .join(" ");

  function onMouseOver(evt: React.MouseEvent<SVGPolygonElement>) {
    if (evt.buttons === 1) {
      (onMouseDownOver as (
        evt: React.MouseEvent<SVGPolygonElement>,
        position: { q: number; r: number }
      ) => any)(evt, { q, r });
    }
  }

  const classes = [
    tileClassPrefix + tileId,
    renderPlayerFog ? "playerFog" : undefined,
    renderMasterFog ? "masterFog" : undefined,
  ]
    .filter((c) => c !== undefined)
    .join(" ");

  return (
    <g className={classes}>
      <polygon
        className="hexagon"
        points={pointsString}
        onClick={
          onClick !== undefined ? (evt) => onClick(evt, { q, r }) : undefined
        }
        onContextMenu={
          onClick !== undefined
            ? (evt) => {
                evt.preventDefault();
                onClick(evt, { q, r });
              }
            : undefined
        }
        onMouseDown={onMouseDownOver !== undefined ? onMouseOver : undefined}
        onMouseOver={onMouseDownOver !== undefined ? onMouseOver : undefined}
        onMouseUp={
          onMouseUp !== undefined
            ? (evt) => onMouseUp(evt, { q, r })
            : undefined
        }
      />
      {tile?.symbol ? (
        <text
          x={center.x}
          y={center.y}
          dominantBaseline="central"
          className="text-xs hexText"
          textAnchor="middle"
        >
          {tile.symbol}
        </text>
      ) : null}
      {renderMasterFog || renderPlayerFog ? (
        <polygon
          className="pointer-events-none hexagon fogOverlay"
          points={pointsString}
        />
      ) : null}
    </g>
  );
});
