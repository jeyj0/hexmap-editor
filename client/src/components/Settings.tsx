import React, { useState } from "react";
import { v4 as uuid } from "uuid";
import { MapType, mapTypes } from "../const";
import { observer } from "mobx-react-lite";
import { useStore } from "../hooks/useStore";
import LoadMapPopup from "./LoadMapPopup";

export default observer(function Settings() {
  const store = useStore();
  const [isEditingName, setIsEditingName] = useState(false);

  const shareUrl =
    store.ws.gameName !== null
      ? window.location.protocol +
        "//" +
        window.location.host +
        "/?player&game=" +
        encodeURIComponent(store.ws.gameName)
      : null;

  const mapDataFile =
    "data:text/json;charset=utf-8," +
    encodeURIComponent(JSON.stringify(store.mapSaveData));

  return (
    <>
      <div>
        {isEditingName ? (
          <form
            onSubmit={(evt) => {
              evt.preventDefault();
              setIsEditingName(false);
            }}
          >
            <input
              type="text"
              autoFocus
              className="nameEdit"
              value={store.mapInfo.name}
              onChange={(evt) => (store.mapInfo.name = evt.target.value)}
              onBlur={() => setIsEditingName(false)}
            />
          </form>
        ) : (
          <button
            className="block font-bold text-left font-heading text-dnd-header"
            onClick={() => setIsEditingName(true)}
          >
            {store.mapInfo.name}
          </button>
        )}
        <select
          onChange={(evt) =>
            (store.mapInfo.mapType =
              mapTypes.get(evt.target.value) ?? MapType.Exploration)
          }
          value={store.mapInfo.mapType}
        >
          {Array.from(mapTypes.entries()).map(([name, type]) => {
            return (
              <option key={name} value={type}>
                {name}
              </option>
            );
          })}
        </select>
        <button
          className="danger"
          onClick={() =>
            store.confirmAction(
              "Are you sure you want to clear all tiles from the map?",
              "Cancel",
              "Clear Map",
              () => store.clearMap()
            )
          }
        >
          Clear Map
        </button>
        <div className="block space-x-2">
          <button
            className="inline-block danger"
            style={{ display: "inline-block" }}
            onClick={() => {
              store.confirmAction(
                `Are you sure you want to save the map as "${store.mapInfo.name}", possibly overwriting the current version?`,
                "Cancel",
                `Save as ${store.mapInfo.name}`,
                () => {
                  store.saveMap();
                }
              );
            }}
          >
            Save Map
          </button>
          <a
            className="inline-block danger"
            style={{ display: "inline-block" }}
            href={mapDataFile}
            download={`${store.mapInfo.name}.json`}
          >
            Download Map Data
          </a>
        </div>
        <div className="block space-x-2">
          <button
            className="danger"
            onClick={() => {
              store.confirmAction(
                `Unsaved changes will be lost. Do you want to load another map?`,
                "Keep Editing",
                "Load another map",
                () => {
                  const popupId = uuid();
                  store.openPopup(<LoadMapPopup popupId={popupId} />, popupId);
                }
              );
            }}
          >
            Load Map
          </button>
          <button
            className="danger"
            onClick={() => store.loadMapFromStringPopup()}
          >
            Load Map From File
          </button>
        </div>
      </div>
      <hr className="w-full my-4" />
      <span className="font-bold font-heading text-dnd-header">
        Configure View
      </span>
      <div className="grid grid-cols-2 gap-2">
        <label>
          <span>Width</span>
          <input
            type="number"
            min="1"
            max="100"
            className="block w-full p-2 bg-backdrop-light"
            value={store.ui.viewport.width}
            onChange={(e) => {
              const w = Number(e.target.value);
              store.ui.viewport.width = isNaN(w) || w <= 0 ? 1 : w;
            }}
          />
        </label>
        <label>
          <span>Height</span>
          <input
            type="number"
            min="1"
            max="100"
            step="1"
            className="block w-full p-2 bg-backdrop-light"
            value={store.ui.viewport.height}
            onChange={(e) => {
              const h = Number(e.target.value);
              store.ui.viewport.height = isNaN(h) || h <= 0 ? 1 : h;
            }}
          />
        </label>
        <label>
          <span>Q Offset</span>
          <input
            type="number"
            step="1"
            className="block w-full p-2 bg-backdrop-light"
            value={store.ui.viewport.offset.q}
            onChange={(e) => {
              const qOffset = Number(e.target.value);
              store.ui.viewport.offset.q = isNaN(qOffset) ? 0 : qOffset;
            }}
          />
        </label>
        <label>
          <span>R Offset</span>
          <input
            type="number"
            step="1"
            className="block w-full p-2 bg-backdrop-light"
            value={store.ui.viewport.offset.r}
            onChange={(e) => {
              const rOffset = Number(e.target.value);
              store.ui.viewport.offset.r = isNaN(rOffset) ? 0 : rOffset;
            }}
          />
        </label>
      </div>
      {shareUrl !== null ? (
        <>
          <hr className="w-full my-4" />
          <span>
            Player URL:
            <a
              href={shareUrl}
              className="block overflow-auto text-blue-700 whitespace-no-wrap"
              target="_blank"
              rel="noopener noreferrer"
            >
              {shareUrl}
            </a>
          </span>
        </>
      ) : null}
    </>
  );
});
