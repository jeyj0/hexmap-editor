import React, { useState, useEffect } from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "../hooks/useStore";
import { Tile } from "../const";

interface TileEditorProps {
  tileId: string;
}

export default observer(function TileEditor({ tileId }: TileEditorProps) {
  const store = useStore();
  const tile = store.tiles.get(tileId);

  const [isEditingName, setIsEditingName] = useState(false);
  const [editingName, setEditingName] = useState(tile?.name ?? "");

  useEffect(() => {
    setEditingName(store.tiles.get(tileId)?.name ?? "");
  }, [tileId, store.tiles]);

  if (tile === undefined) {
    return null;
  }

  function onSubmit(tile: Tile, evt?: React.FormEvent<HTMLFormElement>) {
    if (evt !== undefined) {
      evt.preventDefault();
    }

    if (editingName.length > 0) {
      setIsEditingName(false);
      tile.name = editingName;
    }
  }

  return (
    <div>
      <div className="mb-2">
        {isEditingName ? (
          <form onSubmit={(evt) => onSubmit(tile, evt)}>
            <input
              className="nameEdit"
              autoFocus
              onBlur={() => onSubmit(tile)}
              type="text"
              value={editingName}
              onChange={(evt) => setEditingName(evt.target.value)}
            />
          </form>
        ) : (
          <button
            className="block font-bold text-left font-heading text-dnd-header"
            onClick={() => setIsEditingName(true)}
          >
            {tile.name}
          </button>
        )}
      </div>
      <label className="block">
        <input
          type="color"
          className="w-16 rounded focus:outline-none focus:shadow-outline"
          value={tile.fillColor}
          onChange={(evt) => (tile.fillColor = evt.target.value)}
        />
        <span className="ml-2">Fill Color</span>
      </label>
      <label className="block">
        <input
          type="color"
          className="w-16 rounded focus:outline-none focus:shadow-outline"
          value={tile.color}
          onChange={(evt) => (tile.color = evt.target.value)}
        />
        <span className="ml-2">Symbol Color</span>
      </label>
      <label className="block">
        <input
          type="text"
          className="w-16 p-0 text-center rounded bg-backdrop-light focus:outline-none focus:shadow-outline"
          value={tile.symbol ?? ""}
          onChange={(evt) => (tile.symbol = evt.target.value)}
        />
        <span className="ml-2">Symbol</span>
      </label>
      <button
        onClick={() =>
          store.confirmAction(
            "Are you sure you want to delete this tile?",
            "Cancel",
            "Delete Tile",
            () => store.deleteTile(tileId)
          )
        }
        className="danger"
      >
        Delete Tile
      </button>
    </div>
  );
});
