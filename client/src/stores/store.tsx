import React from "react";
import { observable, action, computed, reaction } from "mobx";
import {
  w,
  h,
  isKToVMap,
  isBoolean,
  isObjectGroup,
  isMapType,
  isMapObject,
  isString,
  objHasOwnProperties,
  tileStyleTagId,
  Mode,
  MapType,
  MapObject,
  ObjectGroup,
  Tile,
  isTile,
  tileClassPrefix,
  localStorageMapPrefix,
  localStorageMapListKey,
  websocketPort,
  websocketHost,
  AssembledMapObject,
} from "../const";
import { v4 as uuid } from "uuid";

interface UiState {
  role: "dungeonMaster" | "player" | null;
  renderFog: boolean;
  animateFog: boolean;
  draggingObject: string | null;
  selectedTile: string | null;
  selectedMode: Mode;
  viewport: {
    offset: {
      q: number;
      r: number;
    };
    width: number;
    height: number;
  };
}

interface StoredMapData {
  defaultTile: Tile;
  tiles: Map<string, Tile>;
  rootObjectGroup: ObjectGroup;
  objectsMap: Map<string, MapObject>;
  objectGroupsMap: Map<string, ObjectGroup>;
  setTiles: Map<string, string>;
  mapInfo: MapInfo;
  fogOfWar: {
    default: boolean;
    overrides: Map<string, boolean>;
  };
}

interface TransmittedGameState {
  defaultTile: Tile;
  tiles: [string, Tile][];
  rootObjectGroup: ObjectGroup;
  objectGroupsMap: [string, ObjectGroup][];
  objectsList: MapObject[];
  setTiles: [string, string][];
  mapInfo: MapInfo;
  fogOfWar: {
    default: boolean;
    overrides: [string, boolean][];
  };
}

function ensureStringKeyedMap<V>(
  obj: any,
  isV: (o: any) => o is V,
  typeName: string
): Map<string, V> {
  if (isKToVMap(obj, isString, isV)) {
    return obj;
  }

  if (!(typeof obj === "object")) {
    throw new Error(
      `Attempted conversion failed. Could not convert to string to ${typeName} map.`
    );
  }

  const map: Map<string, V> = new Map();

  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      const value = obj[key];
      if (isV(value)) {
        map.set(key, value);
      } else {
        console.warn(
          "Could not convert",
          value,
          `to ${typeName}. Skipping in conversion...`
        );
      }
    }
  }

  return map;
}

function attemptConvert(obj: any): StoredMapData {
  if (isStoredMapData(obj)) {
    return obj;
  }

  if (
    !Object.prototype.hasOwnProperty.call(obj, "defaultTile") ||
    !isTile(obj.defaultTile)
  ) {
    throw new Error(
      "Attempted conversion failed. Could not convert defaultTile."
    );
  }

  if (!Object.prototype.hasOwnProperty.call(obj, "tiles")) {
    throw new Error(
      "Attempted conversion failed. Object missing tiles property."
    );
  }
  obj.tiles = ensureStringKeyedMap(obj.tiles, isTile, "Tile");

  if (
    !Object.prototype.hasOwnProperty.call(obj, "rootObjectGroup") ||
    !isObjectGroup(obj.rootObjectGroup)
  ) {
    throw new Error(
      "Attempted conversion failed. Could not convert rootObjectGroup."
    );
  }

  if (!Object.prototype.hasOwnProperty.call(obj, "objectsMap")) {
    throw new Error(
      "Attempted conversion failed. Object missing objectsMap property."
    );
  }
  obj.objectsMap = ensureStringKeyedMap(
    obj.objectsMap,
    isMapObject,
    "MapObject"
  );

  if (!Object.prototype.hasOwnProperty.call(obj, "objectGroupsMap")) {
    throw new Error(
      "Attempted conversion failed. Object missing objectGroupsMap property."
    );
  }
  obj.objectGroupsMap = ensureStringKeyedMap(
    obj.objectGroupsMap,
    isObjectGroup,
    "ObjectGroup"
  );

  if (!Object.prototype.hasOwnProperty.call(obj, "setTiles")) {
    throw new Error(
      "Attempted conversion failed. Object missing setTiles property."
    );
  }
  obj.setTiles = ensureStringKeyedMap(obj.setTiles, isString, "string");

  if (
    !Object.prototype.hasOwnProperty.call(obj, "mapInfo") ||
    !isMapInfo(obj.mapInfo)
  ) {
    throw new Error("Attempted conversion failed. Could not convert mapInfo.");
  }

  function ensureFogOfWar(
    obj: any
  ): { default: boolean; overrides: Map<string, boolean> } {
    if (
      !Object.prototype.hasOwnProperty.call(obj, "default") ||
      !(typeof obj.default === "boolean") ||
      !Object.prototype.hasOwnProperty.call(obj, "overrides")
    ) {
      throw new Error(
        "Attempted conversion failed. Could not convert fogOfWar from save."
      );
    }

    return {
      default: obj.default,
      overrides: ensureStringKeyedMap(obj.overrides, isBoolean, "Boolean"),
    };
  }

  if (!Object.prototype.hasOwnProperty.call(obj, "fogOfWar")) {
    throw new Error("Attempted conversion failed. Could not convert fogOfWar.");
  }
  obj.fogOfWar = ensureFogOfWar(obj.fogOfWar);

  return obj;
}

function isStringToTileMap(obj: any): obj is Map<string, Tile> {
  return isKToVMap(obj, isString, isTile);
}

function isStringToMapObjectMap(obj: any): obj is Map<string, MapObject> {
  return isKToVMap(obj, isString, isMapObject);
}

function isStringToObjectGroupMap(obj: any): obj is Map<string, ObjectGroup> {
  return isKToVMap(obj, isString, isObjectGroup);
}

function isStringToStringMap(obj: any): obj is Map<string, string> {
  return isKToVMap<string, string>(obj, isString, isString);
}

function isStoredMapData(obj: any): obj is StoredMapData {
  if (
    !objHasOwnProperties(obj, [
      "defaultTile",
      "tiles",
      "rootObjectGroup",
      "objectsMap",
      "objectGroupsMap",
      "setTiles",
      "mapInfo",
    ])
  ) {
    return false;
  }

  const baseObj = obj as {
    defaultTile: any;
    tiles: any;
    rootObjectGroup: any;
    objectsMap: any;
    objectGroupsMap: any;
    setTiles: any;
    mapInfo: any;
  };

  if (
    isTile(baseObj.defaultTile) &&
    isStringToTileMap(baseObj.tiles) &&
    isObjectGroup(baseObj.rootObjectGroup) &&
    isStringToMapObjectMap(baseObj.objectsMap) &&
    isStringToObjectGroupMap(baseObj.objectGroupsMap) &&
    isStringToStringMap(baseObj.setTiles) &&
    isMapInfo(baseObj.mapInfo)
  ) {
    return true;
  }

  return false;
}

interface MapInfo {
  id: string;
  name: string;
  mapType: MapType;
}

function isMapInfo(obj: any): obj is MapInfo {
  if (!objHasOwnProperties(obj, ["id", "name", "mapType"])) {
    return false;
  }

  const baseObj = obj as { id: any; name: any; mapType: any };

  return (
    isString(baseObj.id) && isString(baseObj.name) && isMapType(baseObj.mapType)
  );
}

export class Store {
  @observable
  defaultTile: Tile = {
    name: "default",
    color: "black",
    fillColor: "darkgray",
  };

  @observable
  tiles: Map<string, Tile> = new Map();

  @observable
  rootObjectGroup: ObjectGroup = {
    id: uuid(),
    name: "Objects",
    groups: [],
    objects: [],
    mapObjectTemplate: {
      showNameOnMap: true,
      nameColor: "#ffffff",
      color: "#ff0000",
      dmOnly: false,
    },
  };

  @observable
  private objectsMap: Map<string, MapObject> = new Map();

  @observable
  objectGroupsMap: Map<string, ObjectGroup> = new Map([
    [this.rootObjectGroup.id, this.rootObjectGroup],
  ]);

  assembleMapObject(id: string): AssembledMapObject | undefined {
    const mapObject = this.objectsMap.get(id);

    if (mapObject === undefined) {
      return undefined;
    }

    function isAssembled(
      mapObject: MapObject
    ): mapObject is AssembledMapObject {
      return (
        mapObject.showNameOnMap !== null &&
        mapObject.nameColor !== null &&
        mapObject.color !== null &&
        mapObject.dmOnly !== undefined
      );
    }

    if (isAssembled(mapObject)) {
      return JSON.parse(JSON.stringify(mapObject));
    }

    const getParentGroup = (id: string) => {
      for (const group of this.objectGroupsMap.values()) {
        if (group.objects.includes(id) || group.groups.includes(id))
          return group;
      }
      throw new Error("No parent group found");
    };

    const assembledMapObject: MapObject = JSON.parse(JSON.stringify(mapObject));
    let nextId = assembledMapObject.id;
    while (!isAssembled(assembledMapObject)) {
      if (nextId === this.rootObjectGroup.id) {
        return undefined;
      }
      const parent = getParentGroup(nextId);
      if (assembledMapObject.showNameOnMap === null) {
        assembledMapObject.showNameOnMap =
          parent.mapObjectTemplate.showNameOnMap;
      }
      if (assembledMapObject.nameColor === null) {
        assembledMapObject.nameColor = parent.mapObjectTemplate.nameColor;
      }
      if (assembledMapObject.color === null) {
        assembledMapObject.color = parent.mapObjectTemplate.color;
      }
      if (assembledMapObject.dmOnly === undefined) {
        assembledMapObject.dmOnly = parent.mapObjectTemplate.dmOnly;
      }
      nextId = parent.id;
    }

    return assembledMapObject;
  }

  @action
  createObject(objectGroupId: string) {
    const group = this.getObjectGroup(objectGroupId);

    if (group === undefined) {
      throw new Error("Cannot create object in non-existing group");
    }

    const object: MapObject = {
      id: uuid(),
      name: "New Object",
      showNameOnMap: null,
      nameColor: null,
      position: {
        q: 0,
        r: 0,
      },
      color: null,
    };
    this.objectsMap.set(object.id, object);
    group.objects.push(object.id);
    return object;
  }

  getObject(id: string) {
    return this.objectsMap.get(id);
  }

  @computed
  get objectsList() {
    return Array.from(this.objectsMap.values());
  }

  @action
  createObjectGroup(parentGroupId: string) {
    const parent = this.getObjectGroup(parentGroupId);

    if (parent === undefined) {
      throw new Error(
        "Can't create an object group in a non-existant parent group."
      );
    }

    const id = uuid();
    const group: ObjectGroup = {
      id,
      name: "New Object Group",
      groups: [],
      objects: [],
      mapObjectTemplate: {
        showNameOnMap: null,
        color: null,
        nameColor: null,
      },
    };
    this.objectGroupsMap.set(id, group);
    parent.groups.push(id);
    return group;
  }

  getObjectGroup(id: string) {
    return this.objectGroupsMap.get(id);
  }

  @observable
  private popups: Map<string, React.ReactElement> = new Map();

  @action
  openPopup(popup: React.ReactElement, popupId?: string): string {
    const id = popupId ?? uuid();
    this.popups.set(id, popup);
    return id;
  }

  @action
  closePopup(popupId: string) {
    this.popups.delete(popupId);
  }

  @computed
  get popupsList() {
    return Array.from(this.popups.values());
  }

  @action
  confirmAction(
    message: string,
    cancel: string,
    confirm: string,
    action: () => any
  ) {
    const popupId = uuid();
    this.openPopup(
      <form
        onSubmit={(evt) => {
          evt.preventDefault();
          this.closePopup(popupId);
          action();
        }}
        className="bg-gray-800 rounded"
      >
        <div className="p-4 text-dnd-bg">{message}</div>
        <div className="p-2 text-right bg-gray-700 rounded-b space-x-2 font-quote">
          <button
            onClick={() => this.closePopup(popupId)}
            className="p-2 bg-gray-600 border-2 border-gray-800 rounded focus:outline-none focus:shadow-outline active:bg-gray-800"
            autoFocus
          >
            {cancel}
          </button>
          <button
            type="submit"
            className="p-2 border-2 rounded bg-dnd-hr text-dnd-bg border-dnd-header focus:outline-none focus:shadow-outline active:bg-dnd-header"
          >
            {confirm}
          </button>
        </div>
      </form>,
      popupId
    );
  }

  @action
  loadMapFromStringPopup() {
    const popupId = uuid();
    this.openPopup(
      <form
        onSubmit={(evt) => {
          evt.preventDefault();
          this.closePopup(popupId);
          const textareaContent = ((evt.target as HTMLFormElement).elements.namedItem(
            "filecontents"
          ) as HTMLTextAreaElement | undefined)?.value;
          if (isString(textareaContent)) {
            this.loadMapFromString(textareaContent);
          }
        }}
        className="bg-gray-800 rounded"
      >
        <div className="p-4 text-dnd-bg">
          <span className="block">Paste the saved file contents here:</span>
          <textarea name="filecontents" className="text-gray-900" />
        </div>
        <div className="p-2 text-right bg-gray-700 rounded-b space-x-2 font-quote">
          <button
            onClick={() => this.closePopup(popupId)}
            className="p-2 bg-gray-600 border-2 border-gray-800 rounded focus:outline-none focus:shadow-outline active:bg-gray-800"
            autoFocus
          >
            Cancel
          </button>
          <button
            type="submit"
            className="p-2 border-2 rounded bg-dnd-hr text-dnd-bg border-dnd-header focus:outline-none focus:shadow-outline active:bg-dnd-header"
          >
            Load Map
          </button>
        </div>
      </form>,
      popupId
    );
  }

  @observable
  setTiles: Map<string, string> = new Map();

  @observable
  fogOfWar: {
    default: boolean;
    overrides: Map<string, boolean>;
  } = {
    default: true,
    overrides: new Map(),
  };

  @observable
  mapInfo: MapInfo = {
    id: uuid(),
    name: "My Awesome HexMap",
    mapType: MapType.Exploration,
  };

  @observable
  ui: UiState = {
    role: null,
    renderFog: false,
    animateFog: true,
    draggingObject: null,
    selectedTile: null,
    selectedMode: Mode.Paint,
    viewport: {
      offset: { q: 0, r: 0 },
      width: 25,
      height: 19,
    },
  };

  @computed
  get topLeftHex() {
    const zeroZeroHex = { x: w / 2 + 1, y: h / 2 + 1 };

    const q = this.ui.viewport.offset.q;
    const r = this.ui.viewport.offset.r;

    return {
      x: zeroZeroHex.x + (r * w) / 2 + q * w,
      y: zeroZeroHex.y + r * h * 0.75,
    };
  }

  @action
  setAt(q: number, r: number, tileId: string) {
    this.setTiles.set(`${q}|${r}`, tileId);
  }

  @action
  deleteAt(q: number, r: number) {
    this.setTiles.delete(`${q}|${r}`);
  }

  @action
  clearMap() {
    this.setTiles.clear();
  }

  getAt(q: number, r: number) {
    return this.setTiles.get(`${q}|${r}`);
  }

  @action
  addTile() {
    this.tiles.set(uuid(), {
      name: "New Tile",
      color: `#${(Math.random() * 255).toString(16).substr(0, 2)}${(
        Math.random() * 255
      )
        .toString(16)
        .substr(0, 2)}${(Math.random() * 255).toString(16).substr(0, 2)}`,
      fillColor: `#${(Math.random() * 255).toString(16).substr(0, 2)}${(
        Math.random() * 255
      )
        .toString(16)
        .substr(0, 2)}${(Math.random() * 255).toString(16).substr(0, 2)}`,
    });
  }

  @action
  deleteTile(tileId: string) {
    this.tiles.delete(tileId);
    this.setTiles.forEach((mapTileId, k) => {
      if (mapTileId === tileId) {
        this.setTiles.delete(k);
      }
    });
    this.ui.selectedTile =
      this.ui.selectedTile === tileId ? null : this.ui.selectedTile;
  }

  @computed
  get tilesList() {
    return Array.from(this.tiles).map(([id, tile]) => ({ ...tile, id }));
  }

  @computed
  get tileCSS() {
    return (
      `.${tileClassPrefix}default > .hexagon { fill: #111; }
       .${tileClassPrefix}default > .hexText { fill: black; }` +
      this.tilesList
        .map(
          ({ id, fillColor, color }) =>
            `.${tileClassPrefix}${id} > .hexagon {
             fill: ${fillColor};
           }

           .${tileClassPrefix}${id} > .hexText {
             fill: ${color};
           }
           `
        )
        .join("\n")
    );
  }

  get savedMaps(): { id: string; name: string }[] {
    const storage = window.localStorage;

    const mapList = storage.getItem(localStorageMapListKey);

    if (mapList === null) {
      return [];
    } else {
      try {
        const mapIds = JSON.parse(mapList);

        if (!Array.isArray(mapIds)) {
          throw new Error(
            "mapIds are not an array. Cannot get list of saved maps."
          );
        }

        const mapInfos = (mapIds.map((mapId) => {
          const mapData = storage.getItem(localStorageMapPrefix + mapId);

          if (mapData === null) {
            console.debug("No data for map with id", mapId);
            return undefined;
          }

          try {
            const map = JSON.parse(mapData);

            if (
              !Object.hasOwnProperty.call(map, "mapInfo") ||
              !Object.hasOwnProperty.call(map.mapInfo, "name")
            ) {
              console.error("Couldn't get name of map with id", mapId);
              return undefined;
            }

            return { id: mapId, name: map.mapInfo.name };
          } catch (e) {
            console.error(e);
            return undefined;
          }
        }) as ({ id: string; name: string } | undefined)[]).filter(
          (m) => m !== undefined
        ) as { id: string; name: string }[];

        return mapInfos;
      } catch (e) {
        console.error(e);
        return [];
      }
    }
  }

  @action
  loadMap(mapId: string) {
    const storage = window.localStorage;

    const mapDataString = storage.getItem(localStorageMapPrefix + mapId);

    if (mapDataString === null) {
      console.error("No data for map with id", mapId);
      return;
    }

    this.loadMapFromString(mapDataString);
  }

  @action
  loadMapFromString(mapDataString: string) {
    try {
      const mapData = attemptConvert(JSON.parse(mapDataString));

      if (!isStoredMapData(mapData)) {
        throw new Error(`Saved map data is in wrong format. Could not load.`);
      }

      this.defaultTile = mapData.defaultTile;
      this.tiles = mapData.tiles;

      this.rootObjectGroup = mapData.rootObjectGroup;
      if (this.rootObjectGroup.mapObjectTemplate.dmOnly === undefined) {
        // in case I'm loading old data
        this.rootObjectGroup.mapObjectTemplate.dmOnly = false;
      }

      this.objectsMap = mapData.objectsMap;
      this.objectGroupsMap = mapData.objectGroupsMap;
      this.setTiles = mapData.setTiles;
      this.mapInfo = mapData.mapInfo;
      this.fogOfWar = mapData.fogOfWar;
    } catch (e) {
      console.error(e);
      return;
    }
  }

  @computed
  get mapSaveData(): StoredMapData {
    return {
      tiles: this.tiles,
      setTiles: this.setTiles,
      objectsMap: this.objectsMap,
      defaultTile: this.defaultTile,
      objectGroupsMap: this.objectGroupsMap,
      rootObjectGroup: this.rootObjectGroup,
      mapInfo: this.mapInfo,
      fogOfWar: this.fogOfWar,
    };
  }

  saveMap() {
    this.ensureMapIdInMapList();

    const storageKey = localStorageMapPrefix + this.mapInfo.id;

    const data: StoredMapData = this.mapSaveData;

    window.localStorage.setItem(storageKey, JSON.stringify(data));
  }

  ensureMapIdInMapList() {
    const mapId = this.mapInfo.id;
    const storage = window.localStorage;

    const mapList = storage.getItem(localStorageMapListKey);

    if (mapList === null) {
      storage.setItem(localStorageMapListKey, JSON.stringify([mapId]));
    } else {
      try {
        const mapIds = JSON.parse(mapList);

        if (!Array.isArray(mapIds)) {
          throw new Error("mapIds are not an array. Overwriting...");
        }

        if (!mapIds.includes(mapId)) {
          storage.setItem(
            localStorageMapListKey,
            JSON.stringify([...mapIds, mapId])
          );
        }
      } catch (e) {
        console.error(e);
        storage.setItem(localStorageMapListKey, JSON.stringify([mapId]));
      }
    }
  }

  @observable
  ws: {
    socket: WebSocket | null;
    gameName: string | null;
    id: string | null;
    gameId: string | null;
    messageQueue: OutgoingMessage[];
  } = {
    socket: null,
    gameName: null,
    id: null,
    gameId: null,
    messageQueue: [],
  };

  sendMessage(message: OutgoingMessage) {
    this.ws.messageQueue = [...this.ws.messageQueue, message];
  }

  handleSocketMessageMaster(message: IncomingMasterMessage) {
    switch (message.type) {
      case "set-id":
        this.ws.id = message.id;
        break;
      case "game-name-set":
        this.ws.gameName = message.gameName;
        break;
      case "new-player":
        this.sendMessage({
          type: "send-to",
          receiverId: message.socketId,
          message: {
            type: "set-game-state",
            gameState: {
              defaultTile: this.defaultTile,
              tiles: Array.from(this.tiles.entries()),
              setTiles: Array.from(this.setTiles.entries()),
              mapInfo: this.mapInfo,
              rootObjectGroup: this.rootObjectGroup,
              objectGroupsMap: Array.from(this.objectGroupsMap.entries()),
              objectsList: this.objectsList,
              fogOfWar: {
                default: this.fogOfWar.default,
                overrides: Array.from(this.fogOfWar.overrides.entries()),
              },
            },
          },
        });
        break;
    }
  }

  handleSocketMessagePlayer(message: IncomingPlayerMessage) {
    console.debug({ message });
    switch (message.type) {
      case "set-id":
        this.ws.id = message.id;
        break;
      case "game-not-found":
        this.ws.gameName = null;
        break;
      case "game-name-id":
        if (this.ws.gameName === message.gameName) {
          this.ws.gameId = message.gameId;
        }
        break;
      case "set-game-state":
        this.defaultTile = message.gameState.defaultTile;
        this.tiles = new Map(message.gameState.tiles);
        this.setTiles = new Map(message.gameState.setTiles);
        this.mapInfo = message.gameState.mapInfo;
        this.rootObjectGroup = message.gameState.rootObjectGroup;
        this.objectGroupsMap = new Map(message.gameState.objectGroupsMap);
        this.objectsMap = new Map(
          message.gameState.objectsList.map((o) => {
            return [o.id, o];
          })
        );
        this.fogOfWar = {
          default: message.gameState.fogOfWar.default,
          overrides: new Map(message.gameState.fogOfWar.overrides),
        };
        break;
    }
  }
}

interface SetIdMsg {
  type: "set-id";
  id: string;
}

type IncomingMasterMessage = GameNameSetMsg | NewPlayerMsg | SetIdMsg;
interface GameNameSetMsg {
  type: "game-name-set";
  gameName: string;
}
interface NewPlayerMsg {
  type: "new-player";
  gameName: string;
  socketId: string;
}

type IncomingPlayerMessage =
  | GameNotFoundMsg
  | GameNameIdMsg
  | SetIdMsg
  | SetGameStateMsg;
interface GameNotFoundMsg {
  type: "game-not-found";
  gameName: string;
}
interface GameNameIdMsg {
  type: "game-name-id";
  gameName: string;
  gameId: string;
}
interface SetGameStateMsg {
  type: "set-game-state";
  gameState: TransmittedGameState;
}

type OutgoingMessage =
  | StartGameMsg
  | JoinGameMsg
  | BroadcastByGmMsg
  | SendToMsg;
interface StartGameMsg {
  type: "start-game";
}
interface JoinGameMsg {
  type: "join-game";
  gameName: string;
}
interface BroadcastByGmMsg {
  type: "broadcast-by-gm";
  message: IncomingPlayerMessage;
}
interface SendToMsg {
  type: "send-to";
  receiverId: string;
  message: IncomingPlayerMessage | IncomingMasterMessage;
}

export function createStore() {
  const store = new Store();

  reaction(
    () => store.tileCSS,
    (tileCSS) => {
      const styleTag =
        document.getElementById(tileStyleTagId) ||
        (() => {
          const createdTag = document.createElement("style");
          createdTag.id = tileStyleTagId;
          document.head.appendChild(createdTag);
          return createdTag;
        })();

      styleTag.innerHTML = tileCSS;
    },
    {
      fireImmediately: true,
      delay: 10,
    }
  );

  reaction(
    () => store.ui.draggingObject,
    (draggingObject) => {
      if (draggingObject !== null) {
        document.addEventListener(
          "mouseup",
          () => {
            store.ui.draggingObject = null;
          },
          { once: true }
        );
      }
    }
  );

  reaction(
    () => store.ui.role,
    (role) => {
      const ws = new WebSocket(`ws://${websocketHost}:${websocketPort}`);
      store.ws.socket = ws;

      switch (role) {
        case "dungeonMaster":
          ws.addEventListener("message", ({ data }) => {
            store.handleSocketMessageMaster(JSON.parse(data));
          });
          ws.addEventListener("error", (ev) => {
            console.error("WebSocket error evt");
            console.debug(ev);
          });

          store.sendMessage({
            type: "start-game",
          });
          break;
        case "player":
          ws.addEventListener("message", ({ data }) => {
            store.handleSocketMessagePlayer(JSON.parse(data));
          });
          break;
      }
    }
  );

  reaction(
    () => ({
      ws: store.ws.socket,
      wsId: store.ws.id,
      msgs: store.ws.messageQueue,
    }),
    ({ ws, wsId, msgs }) => {
      if (ws !== null && wsId !== null && msgs.length > 0) {
        store.ws.messageQueue = [];
        for (const msg of msgs) {
          ws.send(
            JSON.stringify({
              ...msg,
              id: wsId,
            })
          );
        }
      }
    }
  );

  reaction(
    () => [store.ui.role, store.ws.gameName],
    ([role, gameName]) => {
      if (role === "player" && gameName !== null) {
        store.ws.messageQueue = [
          ...store.ws.messageQueue,
          {
            type: "join-game",
            gameName,
          },
        ];
      }
    }
  );

  reaction(
    () => ({
      role: store.ui.role,
      gameState: JSON.parse(
        JSON.stringify({
          defaultTile: store.defaultTile,
          tiles: Array.from(store.tiles.entries()),
          setTiles: Array.from(store.setTiles.entries()),
          mapInfo: store.mapInfo,
          rootObjectGroup: store.rootObjectGroup,
          objectGroupsMap: Array.from(store.objectGroupsMap.entries()),
          objectsList: store.objectsList,
          fogOfWar: {
            default: store.fogOfWar.default,
            overrides: Array.from(store.fogOfWar.overrides.entries()),
          },
        })
      ),
    }),
    ({ role, gameState }) => {
      if (role === "dungeonMaster") {
        store.sendMessage({
          type: "broadcast-by-gm",
          message: {
            type: "set-game-state",
            gameState,
          },
        });
      }
    },
    {
      delay: 500,
    }
  );

  reaction(
    () => ({
      role: store.ui.role,
      gameName: store.ws.gameName
        ? encodeURIComponent(store.ws.gameName)
        : null,
      animateFog: store.ui.animateFog,
    }),
    ({ role, gameName, animateFog }) => {
      const title =
        (role === "dungeonMaster" ? "DM" : "Player") +
        (gameName !== null ? " (" + decodeURIComponent(gameName) + ")" : "");
      // eslint-disable-next-line no-restricted-globals
      history.replaceState(
        { role, gameName },
        title,
        `?${role}${gameName !== null ? `&game=${gameName}` : ""}${
          animateFog ? "" : "&noAnimate"
        }`
      );
      document.title = title;
    }
  );

  // set as player for game depending on URL
  const search = decodeURIComponent(window.location.search);
  const match = search.match(
    /\?(player|dungeonMaster)(&game=(\w+ \w+ \w+))?(&noAnimate)?/
  );
  if (match !== null) {
    switch (match[1]) {
      case "dungeonMaster":
        store.ui.role = "dungeonMaster";
        store.ws.gameName = match[3];
        break;
      case "player":
        store.ui.role = "player";
        store.ws.gameName = match[3];
        break;
    }
    if (match[4] !== undefined) {
      store.ui.animateFog = false;
    }
  }

  document.addEventListener("keypress", (evt) => {
    const tagName = String.prototype.toUpperCase.call(
      (evt as any)?.originalTarget?.tagName ?? ""
    );
    if (
      !(tagName === "INPUT" || tagName === "SELECT" || tagName === "TEXTAREA")
    ) {
      switch (evt.key) {
        case "s":
          store.ui.selectedMode = Mode.Select;
          break;
        case "p":
          store.ui.selectedMode = Mode.Paint;
          break;
        case "e":
          store.ui.selectedMode = Mode.Erase;
          break;
        case "f":
          store.ui.selectedMode = Mode.FogOfWar;
          break;
      }
    }
  });

  return store;
}
