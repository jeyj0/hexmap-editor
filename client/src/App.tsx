import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import HexMap from "./components/HexMap";
import "./App.css";
import LeftPanel from "./components/LeftPanel";
import RightPanel from "./components/RightPanel";
import { useStore } from "./hooks/useStore";
import { v4 as uuid } from "uuid";

interface RoleSelectorProps {
  popupId: string;
}

const RoleSelector = observer(function RoleSelector({
  popupId,
}: RoleSelectorProps) {
  const store = useStore();

  function setRole(role: "player" | "dungeonMaster") {
    store.ui.role = role;
    store.closePopup(popupId);
  }

  return (
    <div className="p-4 pb-2 bg-gray-800 rounded">
      <span className="text-dnd-bg">I am a...</span>
      <div className="grid grid-cols-2 space-x-2">
        <button className="danger" onClick={() => setRole("dungeonMaster")}>
          Dungeon Master
        </button>
        <button className="danger" onClick={() => setRole("player")}>
          Player
        </button>
      </div>
    </div>
  );
});

export default observer(function App() {
  const store = useStore();

  useEffect(() => {
    if (store.ui.role === null) {
      const popupId = uuid();
      store.openPopup(<RoleSelector popupId={popupId} />, popupId);
    }
    if (store.ui.role === "player" && store.ws.gameName === null) {
      const popupId = uuid();
      store.openPopup(
        <form
          className="p-4 pb-2 bg-gray-800"
          onSubmit={(evt) => {
            evt.preventDefault();
            store.ws.gameName = (evt.currentTarget
              .elements[0] as HTMLInputElement).value;
            store.closePopup(popupId);
          }}
        >
          <label>
            <span className="block text-dnd-bg">
              Enter the game words the DM told you
            </span>
            <input type="text" name="gameName" />
          </label>
          <button type="submit" className="danger">
            Join Game
          </button>
        </form>,
        popupId
      );
    }
  }, [store, store.ui.role, store.ws.gameName]);

  return (
    <div className="font-body">
      <div className={`h-screen overflow-hidden grid App ${store.ui.role}`}>
        {store.ui.role === "dungeonMaster" ? <LeftPanel /> : null}
        <HexMap />
        {store.ui.role === "dungeonMaster" ? <RightPanel /> : null}
      </div>
      {store.popupsList.length > 0 ? (
        <div className="fixed top-0 left-0 w-screen h-screen bg-backdrop-dark" />
      ) : null}
      {store.popupsList.map((popup, idx) => {
        return (
          <div key={idx} className="fixed top-50p left-50p transform-lt-50p">
            {popup}
          </div>
        );
      })}
    </div>
  );
});
