import React, { useContext, createContext } from "react";
import { createStore, Store } from "../stores/store";
import { useLocalStore } from "mobx-react-lite";

const storeContext = createContext<Store | null>(null);

export const StoreProvider = ({
  children,
}: {
  children: React.ReactElement;
}) => {
  const store = useLocalStore(createStore);
  return (
    <storeContext.Provider value={store}>{children}</storeContext.Provider>
  );
};

export function useStore(): Store {
  const store = useContext(storeContext);

  if (!store) {
    throw new Error("useStore must be called within a StoreProvider.");
  }

  return store;
}
